import java.util.ArrayList;
import java.util.Scanner;
	public class Agency
	{
		private String agency;
		private int listings;
		private ArrayList<RealEstateListing> realEstateListingArray = new ArrayList<RealEstateListing>();
		public Agency(String agency, String agencyFile)
		{
		//***	Constructor reads its file into an ArrayList
		//*** don�t forget to close the Scanner
	//*** Here is the code to read a file
	      this.agency = agency; 
		Scanner scanAgencyFile = TextFileIO.createTextRead(agencyFile);
			readFile(scanAgencyFile);
			scanAgencyFile.close();
	       }
		public void readFile(Scanner read)
		{	
		//*** The first number in the file is the number of listings.
		//*** This sets the for loop
		//***	A RealEstateListing can read itself. 	
			listings = read.nextInt();
			read.nextLine();
			for(int count = 0; count < listings; count ++){
				RealEstateListing desiredlist=new RealEstateListing();
				desiredlist.readRealEstateListing(read);
				realEstateListingArray.add(desiredlist);
						
			}
		}
			
		public ArrayList<RealEstateListing> style(String desiredStyle)
		{
		//*** Creates an ArrayList of for this criteria. 	
		//*** Each criteria creates a list.	
			ArrayList<RealEstateListing> desiredArray = new ArrayList<RealEstateListing>();
			//RealEstateListing desiredStylelist = new RealEstateListing();
			for(int index = 0; index<listings; index++){
				if(realEstateListingArray.get(index).getStyle().equalsIgnoreCase(desiredStyle)){
					desiredArray.add(realEstateListingArray.get(index));
				}
			}
			return desiredArray;
			
		}	
		public ArrayList<RealEstateListing> bedrooms(int desiredBedrooms)
		{
		//*** Creates an ArrayList of for this criteria. 	
		//*** Each criteria creates a list.	
			ArrayList<RealEstateListing> desiredArray = new ArrayList<RealEstateListing>();
			//RealEstateListing desiredStylelist = new RealEstateListing();
			for(int index = 0; index<listings; index++){
				if(realEstateListingArray.get(index).getBedrooms()==(desiredBedrooms)){
					desiredArray.add(realEstateListingArray.get(index));
				}
			}
			return desiredArray;
			
		}	
		


	}