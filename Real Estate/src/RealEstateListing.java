import java.text.DecimalFormat;
import java.util.Scanner;

public class RealEstateListing
	{
	       //*** instance variables go here.
		
		DecimalFormat dollarFormat = new DecimalFormat("$#,###,###.##");
		public void readRealEstateListing(Scanner read)
		{
			//*** Read a listing.
			//*** Set the instance variables.
			setListingNumber (read.nextInt());
			read.nextLine();
			setStyle (read.nextLine());
			setBedrooms (read.nextInt());
			read.nextLine();
			setBathrooms (read.nextDouble());
			read.nextLine();
			setLotSize (read.nextDouble());
			read.nextLine();
			setAge (read.nextInt());
			read.nextLine();
			setPrice (read.nextDouble());
			read.nextLine();
			setDistance (read.nextDouble());
			read.nextLine();
			setJurisdiction (read.nextLine());

		}
		//*** I have a complete set of getters and setters created by Eclipse
		private int listingNumber, bedrooms, age;
		public int getListingNumber() {
			return listingNumber;
		}
		public void setListingNumber(int listingNumber) {
			this.listingNumber = listingNumber;
		}
		public int getBedrooms() {
			return bedrooms;
		}
		public void setBedrooms(int bedrooms) {
			this.bedrooms = bedrooms;
		}
		public int getAge() {
			return age;
		}
		public void setAge(int age) {
			this.age = age;
		}
		public double getBathrooms() {
			return bathrooms;
		}
		public void setBathrooms(double bathrooms) {
			this.bathrooms = bathrooms;
		}
		public double getLotSize() {
			return lotSize;
		}
		public void setLotSize(double lotSize) {
			this.lotSize = lotSize;
		}
		public double getPrice() {
			return price;
		}
		public void setPrice(double price) {
			this.price = price;
		}
		public double getDistance() {
			return distance;
		}
		public void setDistance(double distance) {
			this.distance = distance;
		}
		public String getStyle() {
			return style;
		}
		public void setStyle(String style) {
			this.style = style;
		}
		public String getJurisdiction() {
			return jurisdiction;
		}
		public void setJurisdiction(String jurisdiction) {
			this.jurisdiction = jurisdiction;
		}
		private double bathrooms, lotSize, price, distance;
		private String style, jurisdiction;
		public String toString()
		{
			String listing = "Style: "+style+"  Bedroms: "+bedrooms+"  Bathrooms: "+bathrooms+" Lot size: "+lotSize+"  Square acre"
					+ "\nAge: "+"  Price: "+dollarFormat.format(price)+" Distance: "+distance+" Jurisdication: "+jurisdiction;
			return listing;
		}
	}