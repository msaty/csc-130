
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Scanner;
public class RealEstateAgent //*** this is the largest class
{
	//*** ArrayLists that I used.
	Scanner keyboard = new Scanner(System.in);
	private ArrayList<Agency> agencyArray = new ArrayList<Agency>();	
	private ArrayList<RealEstateListing> listingsFromOneAgency = new ArrayList<RealEstateListing>();
	private ArrayList<RealEstateListing> finalArrayListOfRealEstateListings = new ArrayList<RealEstateListing>();
	private String answerSoFar = "";
	private Agency century21 = null;
	private Agency reMax = null;
	private Agency mcEnearney = null;
	private Agency longAndFoster = null;

	public RealEstateAgent()
	{	
		/* Create real-estate companies here.  
	       Give them a name and pass in their text file. 
	       Here is the code.*/
		century21 = new Agency("Century 21","Century21.txt");
		reMax = new Agency("ReMax","ReMax.txt");
		mcEnearney = new Agency("McEnearney","McEnearney.txt");
		longAndFoster = new Agency("longFoster","longFoster.txt");
	}
	public String listingsChosen()
	{
		getAgenciesForTheExhibition();//*** this is an ArrayList
		//*** I read a String and used a Scanner instance to scan a string 
		//*** for the integers integers. See handout for an example of this.


		int homeCriterion = pickHomeCriterion();

		switch (homeCriterion)
		{
		case 1:// Style
			int style = getStyle();
			finalArrayListOfRealEstateListings = style(style, agencyArray);
			break;
			case 2:
			int bedrooms = getBedrooms();
			finalArrayListOfRealEstateListings = bedrooms(bedrooms, agencyArray);
			break;

		default:
			System.out.println("bad topic Selection choice " + homeCriterion);
			System.exit(0);
		}
		houseString();
		return answerSoFar;
	}// switch
	//*** I have a method that creates a string of the agencies used.
	//*** I have a method that takes the final list of homes  
	//*** and puts them in a string that is returned. 


	private int pickHomeCriterion() {
		// TODO Auto-generated method stub
		int Criterion;
		System.out.println("Please choose a home criteria"
				+ "\nand enter its number."
				+ "\n1) Style"
				+ "\n2) Number Of Bedrooms"
				+ "\n3) Number Of Bathrooms"
				+ "\n4) Lot Size"
				+ "\n5) Age"
				+ "\n6) Price"
				+ "\n7) Distance From Work"
				+ "\n8) Jurisdiction");
		Criterion = keyboard.nextInt();
		return Criterion;
	}




	public int getStyle(){
		int Style;
		System.out.println("Please choose the style"
				+ "\nand enter it's number."
				+ "\n1) Colonial"
				+ "\n2) Split Level"
				+ "\n3) Town House"
				+ "\n4) Codominium"
				+ "\n5) Cape Cod"
				+ "\n6) Georgian"
				+ "\n7) Rambler");
		Style = keyboard.nextInt();
		return Style;
	}
	public int getBedrooms(){
		int bedrooms;
		System.out.println("Please enter number of bedrooms desired");
		bedrooms = keyboard.nextInt();
		return bedrooms;
	}

	private void getAgenciesForTheExhibition() {
		// TODO Auto-generated method stub
		System.out.println("For this list of homes which real-estate agencies"
				+ "\nwould you like the home listings drawn from?"
				+ "\nPlease enter their numbers on one line"
				+ "\n1) Century 21"
				+ "\n2) ReMax"
				+ "\n3) McEnearney"
				+ "\n4) Long & Foster");
		String agencies = " "+keyboard.nextLine()+" ";
		if(agencies.contains(" 1 ")){
			agencyArray.add(century21);
		}
		if (agencies.contains(" 2 ")){
			agencyArray.add(reMax);
		}
		if (agencies.contains(" 3 ")){
			agencyArray.add(mcEnearney);
		}
		if (agencies.contains(" 4 ")){
			agencyArray.add(longAndFoster);
		}

	}


	public ArrayList<RealEstateListing> style(int styleType, ArrayList<Agency> agencyArray)
	{
		//*** ask each agency to give a list that satisfies the requested style.
		String houseStyle = "";
		switch (styleType)
		{
		case 1:// Colonial
			houseStyle = "Colonial";
			break;
		case 2:// Split Level
			houseStyle = "Split Level";
			break;
		case 3:// Town House
			houseStyle = "Town House";
			break;
		case 4:// Codominium
			houseStyle = "Codominium";
			break;
		case 5:// Cape Cod
			houseStyle = "Cape Cod";
			break;
		case 6:// Georgian
			houseStyle = "Georgian";
			break;
		case 7:// Rambler
			houseStyle = "Rambler";
			break;
		}
		for(int index = 0; index<agencyArray.size(); index++){
			Agency desiredAgent = agencyArray.get(index);
			listingsFromOneAgency.addAll(desiredAgent.style(houseStyle));
		}
		return listingsFromOneAgency;
	}
	public ArrayList<RealEstateListing> bedrooms(int bedrooms, ArrayList<Agency> agencyArray)
	{
		//*** ask each agency to give a list that satisfies the requested style.
		int houseBedrooms = bedrooms;
		
		for(int index = 0; index<agencyArray.size(); index++){
			Agency desiredAgent = agencyArray.get(index);
			listingsFromOneAgency.addAll(desiredAgent.bedrooms(houseBedrooms));
		}
		System.out.println("flag");
		return listingsFromOneAgency;
	}
	//*** For each criteria there are similar pairs of methods.	
	//*** Work on each pair one pair at a time and make them 
	//*** work before you do the next pair.
	public void houseString(){
		for(int index = 0; index<finalArrayListOfRealEstateListings.size(); index++){
			answerSoFar += finalArrayListOfRealEstateListings.get(index).toString()+"\n";
			System.out.println(finalArrayListOfRealEstateListings.get(index));
		}
	}
}

