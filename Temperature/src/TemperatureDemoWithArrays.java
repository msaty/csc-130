import java.util.Scanner;


public class TemperatureDemoWithArrays {
	public static final int ARRAY_SIZE = 5;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int x;
		Temperature temp1 = new Temperature(120.0, 'C');
		Temperature temp2 = new Temperature(100, 'C');
		Temperature temp3 = new Temperature(50.0, 'C');
		Temperature temp4 = new Temperature(232.0, 'K');
		Temperature tempAve = new Temperature(0.0, 'C');
		Temperature[] tempArray = new Temperature[ARRAY_SIZE];//create pointer to array
		Temperature t1;

		for(x = 0; x < tempArray.length; x++)
		{
			t1 = new Temperature();
			tempArray[x] = t1;// fill the array with Temperatures
		}

		System.out.println("Temp1 is " + temp1);
		//temp1 = temp1.toKelvin();
		temp2.toKelvin();
		System.out.println("Temp1 to Kalvin is " + temp1);
		if (temp1.equals(temp3))
		{
			System.out.println("These two temperatures are equal");
		}
		else
		{
			System.out.println("These two temperature are not equal");
		}
		System.out.println("Temp1 is " + temp1);
		System.out.println("Temp2 is " + temp2);
		System.out.println("Temp3 is " + temp3);
		System.out.println("Temp4 is " + temp4);


		tempAve = tempAve.add(temp1);
		tempAve = tempAve.add(temp2);
		tempAve = tempAve.add(temp3);
		tempAve = tempAve.add(temp4);
		tempAve = tempAve.divide(4);
		System.out.println("the average temperatrure is " + tempAve );

		Temperature[] temperatureArrayOne;
		Temperature[] temperatureArrayTwo;
		Temperature[] temperatureArrayThree;
		temperatureArrayOne = new Temperature[getRandomArraySize()];
		readTemperatureArray(temperatureArrayOne);
		printTemperatureArray(temperatureArrayOne);
		t1 = getAverage(temperatureArrayOne);
		System.out.println("the average of temperature array one is " + t1);
		temperatureArrayTwo = new Temperature[getRandomArraySize()];
		readTemperatureArray(temperatureArrayTwo);
		printTemperatureArray(temperatureArrayTwo);
		t1 = getAverage(temperatureArrayTwo);
		System.out.println("the average of temperature array two is " + t1);
		temperatureArrayThree = new Temperature[getRandomArraySize()];
		readTemperatureArray(temperatureArrayThree);
		printTemperatureArray(temperatureArrayThree);
		t1 = getAverage(temperatureArrayThree);
		System.out.println("the average of temperature array three is " + t1);
		Temperature[] largest = getLargestArray(temperatureArrayOne, temperatureArrayTwo,
				temperatureArrayThree);
		System.out.println();
		Temperature[] arrayWithLargestValues;
		if(temperatureArrayOne == largest)
			arrayWithLargestValues = createArrayWithLargestValues(largest,
					temperatureArrayTwo, temperatureArrayThree);
		else if(temperatureArrayTwo == largest)
			arrayWithLargestValues = createArrayWithLargestValues(largest,
					temperatureArrayOne, temperatureArrayThree);
		else// fractionArrayThree is largest
			arrayWithLargestValues = createArrayWithLargestValues(largest,
					temperatureArrayOne, temperatureArrayTwo);
		System.out.println("An array with the largest values from the 3 arrays is");
		printTemperatureArray(arrayWithLargestValues);
		for(int y = 0; y<temperatureArrayOne.length;y++){
			temperatureArrayOne[y].set(1, 'C');
		}
		printTemperatureArray(arrayWithLargestValues);
	}
	private static void printTemperatureArray(Temperature[] temperatureArrayOne) {
		// TODO Auto-generated method stub
		for (int i = 0; i < temperatureArrayOne.length; i++){
			System.out.println(temperatureArrayOne[i]);
		}
	}
	private static int getRandomArraySize() {
		// TODO Auto-generated method stub
		int size = (int)(Math.random()*5) + 1;
		System.out.println("An array of "+size+" has been created. Fill out the array with "+size+" temperatures");
		return size;
	}
	public static void readTemperatureArray(Temperature[] temperatureArray) {
		// TODO Auto-generated method stub
		
		for (int i = 0; i < temperatureArray.length; i++){
			Temperature t1;
			t1 = new Temperature();
			t1.read();
			temperatureArray[i] = t1;


		}
	}
		public static Temperature getAverage(Temperature[] temperatureArray) 
		{
			// TODO Auto-generated method stub
			Temperature average = new Temperature ();
			double sum = 0;
			for(int i = 0; i < temperatureArray.length; i++){
				temperatureArray[i].toCelsius();
			}
			for(int i = 0; i < temperatureArray.length; i++){
				average=average.add(temperatureArray[i]);
			}
			average=average.divide(temperatureArray.length);
			return average;
		}
		private static Temperature[] getLargestArray(
				Temperature[] temperatureArrayOne,
				Temperature[] temperatureArrayTwo,
				Temperature[] temperatureArrayThree) {
			// TODO Auto-generated method stub
			Temperature[] largestArray ;
			if ((temperatureArrayOne.length > temperatureArrayTwo.length)&&(temperatureArrayOne.length > temperatureArrayThree.length))
				largestArray = temperatureArrayOne;
			else if ((temperatureArrayTwo.length > temperatureArrayOne.length)&&(temperatureArrayTwo.length > temperatureArrayThree.length))
				largestArray = temperatureArrayTwo;
			else
				largestArray = temperatureArrayThree;
			return largestArray;
		}
		private static Temperature[] createArrayWithLargestValues(
				Temperature[] largestArray, Temperature[] temperatureArrayTwo,
				Temperature[] temperatureArrayThree) {
			// TODO Auto-generated method stub
			int size = largestArray.length;
			Temperature[] largest = new Temperature[size];
			Temperature[] smallest;
			Temperature[] middle;
			if(temperatureArrayTwo.length>temperatureArrayThree.length){
				smallest = temperatureArrayThree;
				middle = temperatureArrayTwo;

			}
			else{
				smallest = temperatureArrayTwo;
				middle = temperatureArrayThree;
			}
			for(int index = 0; index<smallest.length; index++){
				if((largestArray[index].getTemp() > middle[index].getTemp())&&(largestArray[index].getTemp() > smallest[index].getTemp()))
					largest[index] = largestArray[index];
				else if((middle[index].getTemp() > largestArray[index].getTemp())&&(middle[index].getTemp() > smallest[index].getTemp()))
					largest[index] = middle[index];
				else 
					largest[index] = smallest[index];
			}
			for(int index = smallest.length; index<middle.length; index++){
				if(largestArray[index].getTemp() > middle[index].getTemp())
					largest[index] =  largestArray[index];
				else 
					largest[index] = middle[index];
			}
			for(int index = middle.length; index<largestArray.length; index++){
				largest[index] = largestArray[index];
			}
			/*for (int index = 0; index<size -1; index++){
				if(vacuumBottleArrayTwo[index].getBottle() > vacuumBottleArrayThree[index].getBottle())
					largest [index] = vacuumBottleArrayTwo [index];
				else
					largest [index] = vacuumBottleArrayThree [index];
				
			}*/
			for(int i = 0; i<largest.length;i++)
				largest[i] = new Temperature(largest[i]);
			return largest;
		}
		/*private static Temperature[] createArrayWithLargestValues(
				Temperature[] largest, Temperature[] temperatureArrayTwo,
				Temperature[] temperatureArrayThree) {
			// TODO Auto-generated method stub
			for (int index = 0; index<temperatureArrayTwo.length -1; index++){
				if(temperatureArrayTwo[index].getTemp() > temperatureArrayThree[index].getTemp())
					largest [index] = temperatureArrayTwo [index];
			}
			return largest;*/
		}
		
	
