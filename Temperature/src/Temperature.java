import java.util.Scanner;


public class Temperature {

	private double temp;
	private char typeTemp;
	
	//public Temperature[](){
		
			
	//}
	
	public Temperature(){
		temp = 0;
		typeTemp = 'N';
		
	}
	
	public Temperature(double d, char c) {
		temp = d;
		typeTemp = c;
	}

	public Temperature(Temperature temperature) {
		// TODO Auto-generated constructor stub
		this.temp = temperature.temp;
		this.typeTemp = temperature.typeTemp;
	}

	public void setTemperature(double temp, char typeTemp){
		setTemp(temp);
		setTypeTemp(typeTemp);
	}
	
	public double getTemp() {
		return temp;
	
	}

	public void setTemp(double temp) {
		this.temp = temp;
	}

	public char getTypeTemp() {
		return typeTemp;
	}

	public void setTypeTemp(char typeTemp) {
		this.typeTemp = typeTemp;
	}

	public Temperature toCelsius() {
		try{
			if(temp < -273.15){
				throw new Exception("Exception: C cannot be < -273.15");
			}
		switch(typeTemp){
		case 'F':
			this.temp = (this.temp-32)*5/9;
			this.typeTemp = 'C';
			break;
		case 'K':
			this.temp = this.temp-273.15;
			this.typeTemp = 'C';
			break;
		}
		}
		catch(Exception e){
			System.out.println(e.getMessage());
			System.out.println("C cannot be < -273.15");
		}
		return this;
		
		
	}
	public String toString()
	{
		return "" + temp +typeTemp;
	}

	public Temperature toKelvin() {
		switch (typeTemp){
		case 'F':
			this.temp = (this.temp-32)*5/9+273.15;
			this.typeTemp = 'K';
			break;
		
		case 'C':
			this.temp = this.temp+273.15;
			this.typeTemp = 'K';
			break;
		}
		/*if(temp < 0){
			System.out.println("K cannot be < 0");
			System.exit(0);
		}*/
		return this;
		
	}
	public Temperature toFahrenheit() {
		switch (typeTemp){
		case 'K':
			this.temp = (this.temp-273.15)*9/5+32;
			this.typeTemp = 'F';
			break;
		
		case 'C':
			this.temp = this.temp*9/5+32;
			this.typeTemp = 'F';
			break;
		}
		if(temp < -459.67){
			System.out.println("F cannot be < -459.67");
			System.exit(0);
		}
		return this;
		
	}

	public Temperature add(Temperature temp) {
		Temperature temp1 = this.toCelsius();
		Temperature temp2 = temp.toCelsius();
		
		return new Temperature(temp1.temp+temp2.temp, 'C');
	}

	public Temperature divide(double i) {
		Temperature answer = this.toCelsius();
		double newTemp = (double)(answer.temp/i);
		return new Temperature(newTemp, 'C');
	}

	public Temperature subtract(Temperature temp) {
		Temperature temp1 = this.toCelsius();
		Temperature temp2 = temp.toCelsius();
		
		return new Temperature(temp1.temp-temp2.temp, 'C');
	}
	public Temperature multiply(Temperature temp) {
		Temperature temp1 = this.toCelsius();
		Temperature temp2 = temp.toCelsius();
		
		return new Temperature(temp1.temp*temp2.temp, 'C');
	}
	public boolean equals(Temperature temp){
		Temperature tempor = new Temperature(0.0, 'N');
		if (this.temp==temp.temp&&this.typeTemp==temp.typeTemp)
			return true;
		else if(this.typeTemp!=temp.typeTemp){
			if (temp.typeTemp == 'C')
			tempor = this.toCelsius();
			else if (temp.typeTemp == 'F')
				tempor = this.toFahrenheit();
			else if (temp.typeTemp == 'K')
				tempor = this.toKelvin();
		if(tempor.temp==temp.temp)
			return true;
		else 
			return false;
		
		}
		return false;
		
	}
	public boolean greaterThan(Temperature temp){
	
	if(this.temp>temp.temp)
		return true;
	else
		return false;
	}
	public void read(){
	Scanner keyboard = new Scanner(System.in);
	System.out.println("Please enter in a temp");
	temp = keyboard.nextDouble();
	System.out.println("Please enter the temp type");
	typeTemp = keyboard.next().charAt(0);
	
	
	
	}

	public void set(int i, char c) {
		// TODO Auto-generated method stub
		this.temp = i;
		this.typeTemp = c;
	}
}


