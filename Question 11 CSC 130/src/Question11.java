
import java.util.Scanner;

public class Question11 
{
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String binary;
		
		System.out.println("Enter a 4 digit binary number and press the ENTER key:");
		Scanner keyboard = new Scanner(System.in);
		binary = keyboard.next();
		//collecting the binary number as a string variable
		
		String b0 = binary.substring(0,1);
		String b1 = binary.substring(1,2);
		String b2 = binary.substring(2,3);
		String b3 = binary.substring(3,4);
		//splitting the string into substrings
		
		int decB0 = Integer.parseInt(b0);
		int decB1 = Integer.parseInt(b1);
		int decB2 = Integer.parseInt(b2);
		int decB3 = Integer.parseInt(b3);
		//converting the substrings into integers
		
		int decimal = decB0 * 8 + decB1 * 4 + decB2 * 2 + decB3 * 1;
		//converts binary number into decimal form by multiplying each 
		//substring by the appropriate value
		System.out.print("Your number in decimal form is: "+decimal);
	
		
	}

}
