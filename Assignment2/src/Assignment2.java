import java.util.Scanner;
import java.util.Arrays;

public class Assignment2 {

	public static void main(String[] args) {
		//Initialize new array
		double[] number = new double[5]; 
	 
		Scanner keyboard = new Scanner(System.in);
		//Prompt user for input of five numbers
		System.out.println("Enter five numbers pressing the ENTER key after each number:");
		double sum = 0;
		//Place five numbers into index using a for loop
		for (int index = 0; index <5; index++)
		{
			number[index] = keyboard.nextDouble();
			sum = sum + number[index];
		}
		//Initialize the max and min as the first number in the array and compute the average
		double max = number[1];
		double min = number[1];
		double average = sum / 5;
		//Calculate the max and min value using a for loop
		for (int index = 0; index <5; index++)
		{
			if (number[index] > max)
					max = number[index];
		}
		for (int index = 0; index <5; index++)
		{
			if (number[index] < min)
					min = number[index];
		}
		//Use the sort statement to place the numbers in order
		Arrays.sort(number);
		//Print out the number data
		System.out.println("The sum of the numbers is: "+sum);
		System.out.println("The average of the numbers is: "+average);
		System.out.println("The max of the numbers is: "+max);
		System.out.println("The min of the numbers is: "+min);
		System.out.println("The median of the numbers is: "+number[3]);

		

	}

}
