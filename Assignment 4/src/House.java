
/**
 * @author Matt
 *
 */
public class House {

		public static void main(String[] args){
			/**
			 * creates and outputs a room with default arguments.
			 */
			Room Room0 = new Room();
			Room0.writeOutput();
			/**
			 * creates and outputs a room that has yellow walls, hardwood floors and one window
			 */
			Room Room1 = new Room("yellow", "hardwood", 1,1);
			Room1.writeOutput();
			/**
			 * creates and outputs a room that has purple walls, tile floors, and no windows
			 */
			Room Room2 = new Room("purple", "tile", 0, 2);
			Room2.writeOutput();
			/**
			 * creates and outputs a room that has white walls, carpet floors, and no windows
			 */
			Room Room3 = new Room("white", "carpet", 3, 3);
			Room3.writeOutput();
			
		}
}
