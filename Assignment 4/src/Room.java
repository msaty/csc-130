
/**
 * @author Matt
 *
 */
/**
 * creates public class room for class "House" to access and build rooms
 */
public class Room {
/**
 * makes variables only available to this class
 */
	private String roomColor;
	private String roomFloor;
	private int roomWindows;
	private int roomNumber;
/**
 * creates overloaded constructors for each room case
 * constructor if no arguments are set
 */
	public Room(){
		this.roomColor = "white:";
		this.roomFloor = "carpet";
		this.roomWindows = 0;
		this.roomNumber = 0;
	}
	/**
	 * constructor if one argument is set
	 * @param color if only color is set it will use that value
	 */
	public Room(String color){
		this.roomColor = color;
		this.roomFloor = "carpet";
		this.roomWindows = 0;
		this.roomNumber = 0;
	}
	/**
	 * constructor if two arguments are set
	 * @param color if color is set it will use that value
	 * @param floor if floor is set it will use that value
	 */
	public Room(String color, String floor){
		this.roomColor = color;
		this.roomFloor = floor;
		this.roomWindows = 0;
		this.roomNumber = 0;
	}
	/**
	 * constructor if three arguments are set
	 * @param color if color is set it will use that value
	 * @param floor if floor is set it will use that value
	 * @param windows if windows is set it will use that value
	 */
	public Room(String color, String floor, int windows){
		this.roomColor = color;
		this.roomFloor = floor;
		this.roomWindows = windows;
		this.roomNumber = 0;
	}
	/**
	 * constructor if four arguments are set
	 * @param color if color is set it will use that value
	 * @param floor if floor is set it will use that value
	 * @param windows if windows is set it will use that value
	 * @param number if number is set it will use that value
	 */
	public Room(String color, String floor, int windows, int number){
		this.roomColor = color;
		this.roomFloor = floor;
		this.roomWindows = windows;
		this.roomNumber = number;
	}
	/**
	 * set method for the entire room
	 * @param color will run method setColor
	 * @param floor will run method setFloor 
	 * @param windows will run method setWindows
	 * @param number 
	 */
	public void setRoom(String color, String floor, int windows, int number){
		setColor(color);
		setFloor(floor);
		setWindows(windows);
		setNumber(number);
	}
	/**
	 * will take argument and place it into variable color
	 * @param color will return color value
	 */
	public void setColor(String color){
		roomColor = color;
	}
	/**
	 * will take argument and place it into variable floor
	 * @param floor will return floor value
	 */
	public void setFloor(String floor){
		roomFloor = floor;
	}
	/**
	 * will take argument and place it into variable windows
	 * @param windows will return amount of windows
	 */
	public void setWindows(int windows){
		roomWindows = windows;
	}
	/**
	 * will take argument and place it into variable number
	 * @param number will return room number
	 */
	public void setNumber(int number){
		roomNumber = number;
	}
	/**
	 * will return value from setColor
	 * @returnget color to variable
	 */
	public String getColor(){
		return roomColor;
	}
	/**
	 * will return value from setFloor
	 * @return floor to variable
	 */
	public String getFloor(){
		return roomFloor;
	}
	/**
	 * will return value from getWindows
	 * @return windows to variable
	 */
	public int getWindows(){
		return roomWindows;
	}
	/**
	 * will return value from getNumber
	 * @return Number to variable
	 */
	public int getNumber(){
		return roomNumber;
	}
	/**
	 * writes out the room setup
	 */
	public void writeOutput()
	{
		System.out.println("Room "+roomNumber+"'s Color is: "+roomColor);
		System.out.println("Room "+roomNumber+"'s Floor type is: "+roomFloor);
		System.out.println("Room "+roomNumber+"'s Window count is: "+roomWindows);
	}
	
}

