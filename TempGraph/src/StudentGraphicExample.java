
import java.util.Scanner;	

import javax.swing.JApplet;

import java.awt.*;

public class StudentGraphicExample extends JApplet
{
	int count = 10;
	int forColor = 1;
	Scanner scanFile = TextFileIO.createTextRead("washingtonTemperatures.txt");
	int[] months = {31,28,31,30,31,30,31,31,30,31,30,31};
	int[] highestAverage = new int[12];
	int[] lowestAverage = new int[12];
	
	public void paint (Graphics page)
   {
		//setBackground(Color.WHITE);
		//tempData mainTemp = new tempData();
		//mainTemp.readData(scanFile);
		 String out1 = "Jan";
		 String out2 = "Feb";
		 String out3 = "Mar";
		 String out4 = "Apr";
		 String out5 = "May";
		 String out6 = "Jun";
		 String out7 = "Jul";
		 String out8 = "Aug";
		 String out9 = "Sep";
		 String out10 = "Oct";
		 String out11 = "Nov";
		 String out12 = "Dec";
		 String out13 = "40";
		 String out14 = "0";
		 String out15 = "10";
		 String out16 = "20";
		 String out17 = "30";
		 String out18 = "50";
		 String out19 = "60";
		 String out20 = "70";
		 String out21 = "80";
		 String out22 = "90";
		 String out23 = "100";
		 String out24 = "Washington Temperatures";


		page.setColor(Color.BLACK);
		page.drawString(out1, 33, 620);
		page.drawString(out2, 90, 620);
		page.drawString(out3, 152, 620);
		page.drawString(out4, 214, 620);
		page.drawString(out5, 278, 620);
		page.drawString(out6, 332, 620);
		page.drawString(out7, 398, 620);
		page.drawString(out8, 466, 620);
		page.drawString(out9, 530, 620);
		page.drawString(out10, 592, 620);
		page.drawString(out11, 655, 620);
		page.drawString(out12, 718, 620);
		page.drawString(out13, 780, (600-5*(highestAverage[0]-3)));
		page.drawString(out14, 780, (600-5*(highestAverage[0]-43)));
		page.drawString(out15, 780, (600-5*(highestAverage[0]-33)));
		page.drawString(out16, 780, (600-5*(highestAverage[0]-23)));
		page.drawString(out17, 780, (600-5*(highestAverage[0]-13)));
		page.drawString(out18, 780, (600-5*(highestAverage[0]+7)));
		page.drawString(out19, 780, (600-5*(highestAverage[0]+17)));
		page.drawString(out20, 780, (600-5*(highestAverage[0]+27)));
		page.drawString(out21, 780, (600-5*(highestAverage[0]+37)));
		page.drawString(out22, 780, (600-5*(highestAverage[0]+47)));
		page.drawString(out23, 780, (600-5*(highestAverage[0]+57)));
		page.drawString(out24, 300, 60);

		graph(page);
	}
	public void graph(Graphics page)
	{
		Scanner scanFile = TextFileIO.createTextRead("washingtonTemperatures.txt");
		for(int count = 0; count<months.length; count++){
			int averageHigh = 0, averageLow = 0;
			for(int index = 0; index<months[count]; index++){
				scanFile.nextInt();
				averageHigh += scanFile.nextInt();
				averageLow += scanFile.nextInt();
				scanFile.nextLine();
			}
			highestAverage[count] = averageHigh/months[count];
			lowestAverage[count] = averageLow/months[count];
			//System.out.println(highestAverage[count]);
			//System.out.println(lowestAverage[count]);
			int x;
			page.setColor(Color.BLACK);
			setNewColor(page, 3);
			for(x= 10; x < 41; x++)
			{
				page.drawLine (x, 600, x, 600-5*highestAverage[0]);  // Jan
			}
			setNewColor(page, 2);
			for(x= 42; x < 73; x++)
			{
				page.drawLine (x, 600, x, 600-5*lowestAverage[0]);  // Jan
			}
			setNewColor(page, 3);
			for(x= 74; x < 102; x++)
			{
				page.drawLine (x, 600, x, 600-5*highestAverage[1]);  // Feb
			}
			setNewColor(page, 2);
			for(x= 103; x < 131; x++)
			{
				page.drawLine (x, 600, x, 600-5*lowestAverage[1]);  // Feb
			}
			setNewColor(page, 3);
			for(x= 132; x < 163; x++)
			{
				page.drawLine (x, 600, x, 600-5*highestAverage[2]);  // Mar
			}
			setNewColor(page, 2);
			for(x= 164; x < 195; x++)
			{
				page.drawLine (x, 600, x, 600-5*lowestAverage[2]);  // Mar
			}
			setNewColor(page, 3);
			for(x= 196; x < 226; x++)
			{
				page.drawLine (x, 600, x, 600-5*highestAverage[3]);  // Apr
			}
			setNewColor(page, 2);
			for(x= 227; x < 257; x++)
			{
				page.drawLine (x, 600, x, 600-5*lowestAverage[3]);  // Apr
			}
			setNewColor(page, 3);
			for(x= 258; x < 289; x++)
			{
				page.drawLine (x, 600, x, 600-5*highestAverage[4]);  // May
			}
			setNewColor(page, 2);
			for(x= 290; x < 321; x++)
			{
				page.drawLine (x, 600, x, 600-5*lowestAverage[4]);  // May
			}
			setNewColor(page, 3);
			for(x= 322; x < 352; x++)
			{
				page.drawLine (x, 600, x, 600-5*highestAverage[5]);  // Jun
			}
			setNewColor(page, 2);
			for(x= 353; x < 383; x++)
			{
				page.drawLine (x, 600, x, 600-5*lowestAverage[5]);  // Jun
			}
			setNewColor(page, 3);
			for(x= 384; x < 415; x++)
			{
				page.drawLine (x, 600, x, 600-5*highestAverage[6]);  // Jul
			}
			setNewColor(page, 2);
			for(x= 416; x < 447; x++)
			{
				page.drawLine (x, 600, x, 600-5*lowestAverage[6]);  // Jul
			}
			setNewColor(page, 3);
			for(x= 448; x < 479; x++)
			{
				page.drawLine (x, 600, x, 600-5*highestAverage[7]);  // Aug
			}
			setNewColor(page, 2);
			for(x= 480; x < 511; x++)
			{
				page.drawLine (x, 600, x, 600-5*lowestAverage[7]);  // Aug
			}
			setNewColor(page, 3);
			for(x= 512; x < 542; x++)
			{
				page.drawLine (x, 600, x, 600-5*highestAverage[8]);  // Sept
			}
			setNewColor(page, 2);
			for(x= 543; x < 573; x++)
			{
				page.drawLine (x, 600, x, 600-5*lowestAverage[8]);  // Sept
			}
			setNewColor(page, 3);
			for(x= 574; x < 605; x++)
			{
				page.drawLine (x, 600, x, 600-5*highestAverage[9]);  // Oct
			}
			setNewColor(page, 2);
			for(x= 606; x < 637; x++)
			{
				page.drawLine (x, 600, x, 600-5*lowestAverage[9]);  // Oct
			}
			setNewColor(page, 3);
			for(x= 638; x < 668; x++)
			{
				page.drawLine (x, 600, x, 600-5*highestAverage[10]);  // Nov
			}
			setNewColor(page, 2);
			for(x= 669; x < 699; x++)
			{
				page.drawLine (x, 600, x, 600-5*lowestAverage[10]);  // Nov
			}
			setNewColor(page, 3);
			for(x= 700; x < 731; x++)
			{
				page.drawLine (x, 600, x, 600-5*highestAverage[11]);  // Dec
			}
			setNewColor(page, 2);
			for(x= 732; x < 763; x++)
			{
				page.drawLine (x, 600, x, 600-5*lowestAverage[11]);  // Dec
			}
			setNewColor(page, 1);
			//page.drawLine (700, 400, 10, 400);  // line
		     //page.drawRect (50, 50, 40, 40);    // square
		     page.drawRect (10, 80, 764, 520);   // rectangle
             //page.drawOval (175, 65, 80, 80);    // circle
            	}
	}
		
	private void setNewColor(Graphics page, int forColor)
	{
		switch (forColor)
		{
			case 1:
			page.setColor(Color.BLACK);
			break;
			case 2:
			page.setColor(Color.BLUE);
			break;
			case 3:
			page.setColor(Color.RED);
			break;
			case 4:
			page.setColor(Color.DARK_GRAY);
			break;
			case 5:
			page.setColor(Color.GRAY);
			break;
			case 6:
			page.setColor(Color.GREEN);
			break;
			case 7:
			page.setColor(Color.LIGHT_GRAY);
			break;
			case 8:
			page.setColor(Color.DARK_GRAY);
			break;
			case 9:
			page.setColor(Color.MAGENTA);
			break;
			case 10:
			page.setColor(Color.ORANGE);
			break;
			case 11:
			page.setColor(Color.PINK);
			break;
			case 12:
			page.setColor(Color.YELLOW);
			break;
		}
	}
 }
