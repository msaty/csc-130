
public class ExceededMaxException extends Exception {
	public ExceededMaxException(){
		super("Exceeded max value!");
	}
}
