
public class DivideByZeroException extends Exception {
	public DivideByZeroException(){
		super("Using a negative number!");
	}
}
