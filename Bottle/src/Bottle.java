import java.util.Scanner;

public class Bottle {
	private int mM;

	public void read() {
		// TODO Auto-generated method stub
		try {
			Scanner keyboard = new Scanner(System.in);
			mM = keyboard.nextInt();
			if (mM < 0)
				throw new NegativeNumberException();
		} catch (NegativeNumberException e) {
			System.out.println("Did not enter a positive number for Bottle");
			System.exit(0);
		}

	}

	public void setBottle(int bottleValue) {
		bottleValue = mM;

	}

	public int getBottle() {
		return mM;
	}

	public String toString() {
		return "" + mM;
	}

	public Bottle add(Bottle bottle) {
		// TODO Auto-generated method stub
		Bottle answer = new Bottle();
		try {

			if (answer.mM > 500)
				throw new ExceededMaxException();
			answer.mM = this.mM + bottle.mM;

		} catch (ExceededMaxException e) {
			System.out.println("exceeded max value in add Bottle");
			System.exit(0);
		}
		return answer;
	}

	public Bottle divide(Bottle bottle) {
		Bottle answer = new Bottle();
		try {
			answer.mM = this.mM / bottle.mM;
			if (answer.mM < 0)
				throw new NegativeNumberException();
			if (answer.mM < 1)
				throw new DivideByZeroException();
		} catch (NegativeNumberException e) {
			System.out.println("Did not enter a positive number for Bottle");
			System.exit(0);
		}

		catch (DivideByZeroException e) {
			System.out.println("Cannot divide by 0");
			System.exit(0);
		}
		return answer;
	}

	public Bottle divide(int i) {
		Bottle answer = new Bottle();
		try {
			answer.mM = this.mM / i;
			if (answer.mM < 0)
				throw new NegativeNumberException();
			if (answer.mM < 1)
				throw new DivideByZeroException();
		} catch (NegativeNumberException e) {
			System.out.println("Did not enter a positive number for Bottle");
			System.exit(0);
		}

		catch (DivideByZeroException e) {
			System.out.println("Cannot divide by 0");
			System.exit(0);
		}
		return answer;
	}

	public Bottle subtract(Bottle bottle) {
		Bottle answer = new Bottle();
		try {
			answer.mM = this.mM - bottle.mM;
			if (answer.mM < 0)
				throw new NegativeNumberException();

		} catch (NegativeNumberException e) {
			System.out.println("Did not enter a positive number for Bottle");
			System.exit(0);
		}
		return answer;
	}

	public Bottle subtract(int x) {
		Bottle answer = new Bottle();
		try {
			answer.mM = this.mM - x;
			if (answer.mM < 0)
				throw new NegativeNumberException();

		} catch (NegativeNumberException e) {
			System.out.println("Did not enter a positive number for Bottle");
			System.exit(0);
		}
		return answer;
	}

	public void setmM(int i) {
		mM = i;

	}

	public Bottle multiply(Bottle bottle) {
		Bottle answer = new Bottle();
		try {
			answer.mM = this.mM * bottle.mM;
			if (answer.mM < 0)
				throw new NegativeNumberException();
			if (answer.mM > 500)
				throw new ExceededMaxException();

		} catch (ExceededMaxException e) {
			System.out.println("exceeded max value in Bottle");
			System.exit(0);
		} catch (NegativeNumberException e) {
			System.out.println("Entered in negative number for multiply");
			System.exit(0);
		}
		return answer;
	}

	public Bottle multiply(int i) {
		Bottle answer = new Bottle();
		try {
			answer.mM = this.mM * i;
			if (answer.mM < 0)
				throw new NegativeNumberException();
			if (answer.mM > 500)
				throw new ExceededMaxException();

		} catch (ExceededMaxException e) {
			System.out.println("exceeded max value in Bottle");
			System.exit(0);
		} catch (NegativeNumberException e) {
			System.out.println("Entered in negative number for multiply");
			System.exit(0);
		}
		return answer;
	}

	public Bottle add(int x) {
		Bottle answer = new Bottle();
		try {
			if (answer.mM > 500)
				throw new ExceededMaxException();
			answer.mM = this.mM + x;

		} catch (ExceededMaxException e) {
			System.out.println("exceeded max value in add Bottle");
			System.exit(0);
		}

		return answer;
	}

	public boolean equals(Bottle bottle) {

		return this.mM == bottle.getBottle();

	}
}
