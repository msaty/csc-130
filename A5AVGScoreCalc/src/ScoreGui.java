import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
/**
 * Builds Class ScoreGui for Score calculator to access
 * @author Matt
 *
 */
public class ScoreGui extends JFrame implements ActionListener {
	final int SIZE=7;
	JButton jbtCalculate = new JButton("Calculate");
	//scores text fields
	JTextField [] jtxtScore = new JTextField[SIZE];
	JTextField [] jtxtWeight = new JTextField[SIZE];
	JTextField jtxtName = new JTextField();
	JLabel [] jlblModuleName = new JLabel [SIZE];
	//jlblModuleName = {"Student Name", "", "Assignemnets", "Midterm Exam", "Final Project", "Final Exam", ""};
	JLabel jlblOutput = new JLabel("0");
	JPanel scorePanel = new JPanel();
	JPanel buttonPanel = new JPanel();
	
	double [] scores = new double[SIZE];//use for scoreCalculator method;
	double [] weights = new double[SIZE];//use for scoreCalculator method
	
	
	/**
	 * Constructs the graphic user interface for score calculator
	 */
	
	public ScoreGui(){
		jlblModuleName[0] = new JLabel("Student Name");
		jlblModuleName[1] = new JLabel("Assignments");
		jlblModuleName[2] = new JLabel("Midterm Exam");
		jlblModuleName[3] = new JLabel("Final Project");
		jlblModuleName[4] = new JLabel("Final Exam");
		jlblModuleName[5] = new JLabel("Grade Average");
		jlblModuleName[6] = new JLabel("Letter Grade");


		
		scorePanel.setLayout(new GridLayout(7,3));
	
		/**
		 * Adds action listener for each of the panels
		 */
		jbtCalculate.addActionListener(this);
		for (int i=0; i<SIZE; i++){
			scorePanel.add(jlblModuleName[i]);
			jtxtScore[i] = new JTextField(8);
			scorePanel.add(jtxtScore[i]);
			jtxtWeight[i] = new JTextField(8);
			scorePanel.add(jtxtWeight[i]);
		}
		/**
		 * Makes the button calculate the scores
		 */
		buttonPanel.add(jbtCalculate);
		add(scorePanel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
		/*add(jtxtName);
		add(jbtCalculate);
		add(jlblOutput);
		*/
		setVisible(true);
		pack();
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	/**
	 * Calculate grade average scores
	 * @param score an array of scores
	 * @param weight an array of weights
	 * @return grade average score
	 */
	public double calculateScore(double []score, double []weight){
		double grade=0.0;
		//logic to calculate
		//need to modify the code below to support the correct logic
		grade = ((score[1]*weight[1])+(score[2]*weight[2])+(score[3]*weight[3])+(score[4]*weight[4]));
		
		
		return grade;
	}
	/**
	 * Calculate Letter grade
	 * @param score 
	 * @return letter grade A, B, C, D, F
	 */
	public String caculateLetterGrade(double grade) {
		String letter="N";
		if ((grade<=100.0) && (grade>=90.0))
			letter = "A";
		if ((grade<=89.9999) && (grade>=80.0))
			letter = "B";
		if ((grade<=79.9999) && (grade>=70.0))
			letter = "C";
		if ((grade<=69.9999) && (grade>=60.0))
			letter = "D";	
		if (grade<=59.9999)
			letter = "F";
			return letter;
		
		}
		
	
	/**
	 * Action that is performed when button is pushed
	 */
	public void actionPerformed(ActionEvent e){
		//jlblOutput.setText(jtxtName.getText());
		//parse string to double for calculation
		/**
		 * collects numbers from specific scores and weights array to calculate the grade average
		 */
		for(int i=1; i<5; i++){
		scores[i] = Double.parseDouble(jtxtScore[i].getText());
		weights[i] = Double.parseDouble(jtxtWeight[i].getText());
		}
		double grade = calculateScore(scores, weights);
		jtxtScore[6].setText(caculateLetterGrade(grade));
		jtxtScore[5].setText(calculateScore(scores,weights)+"");
		
}
}
