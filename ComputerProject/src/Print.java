import javax.swing.JOptionPane;
/**
 * creates class Print for Computer to access and print out the the computer configuration, shipping info, and cost
 * @author Matt
 *
 */
public class Print {
	/**
	 * 
	 * @param user takes user information and passes it to write class
	 * @param components takes components selected and passes it to write class
	 * @param cost takes total cost of the computer and passes it to write class
	 */
		
	public void writeOutput(String user[], String components[], double cost) {
		// TODO Auto-generated method stub
		JOptionPane.showMessageDialog(null, "Your PC Components are:\n"+
		"CPU: "+components[0]+"\nGPU: "+components[1]+
		"\nHard Drive: "+components[2]+
		"\nMemory: "+components[3], "My PC Info", JOptionPane.INFORMATION_MESSAGE);
		JOptionPane.showMessageDialog(null, "Your Shipping Information is:\n"+
				"Name: "+user[2]+" "+user[3]+"\nStreet Address: "+user[4]+
				"\nCity: "+user[5]+
				"\nState: "+user[6]+
				"\nZip Code: "+user[7], "My Shipping Info", JOptionPane.INFORMATION_MESSAGE);
		JOptionPane.showMessageDialog(null, "Your PC Cost is: $"+cost, "Your PC Cost", JOptionPane.INFORMATION_MESSAGE);
	}

}
