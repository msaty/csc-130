import javax.swing.JOptionPane;


public class Authentic {
	
	public static String Authentication(String[] user){
		int attempt = 0;
		String pass = "";
		String username = "";
		while (!username.equalsIgnoreCase(user[0])){
		username = JOptionPane.showInputDialog("Enter your Username, then press the ENTER key: ");
		}
		JOptionPane.showMessageDialog(null, "Welcome "+user[0]);
		while  (attempt < 3 && !pass.equalsIgnoreCase(user[1])) {	
			pass = JOptionPane.showInputDialog(
						"Enter your password, then press the ENTER key:");
			attempt++;
		}
		
		if (attempt == 3){
			JOptionPane.showMessageDialog(null, "Incorrect credentials, contact admin.");
			System.exit(0);
		}
		return pass;
}
}
