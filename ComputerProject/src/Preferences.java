import javax.swing.JFrame;
import javax.swing.JOptionPane;
/**
 * creates public class Preferences for class Computer to access and select the user's brand preferences
 * @author Matt
 *
 */

public class Preferences {

	public String[] specs = new String[4];
	
	/**
	 * Ask the user's brand preferences and returns them in array Specs
	 * @return Returns array Specs
	 */
	public String[] pickSpecs(){
		/**
		 * Asks user if they prefer AMD or Intel via a dropdown menu
		 */
		String [] cpuChoice = {"AMD","INTEL"};
		JFrame cpuFrame = new JFrame();
		/**
		 * Stores choice into specs[0]
		 */
		specs[0] = (String)JOptionPane.showInputDialog(cpuFrame, "Select a CPU", "Select CPU Brand", JOptionPane.QUESTION_MESSAGE, null, cpuChoice, cpuChoice[1]);
		/**
		 * Asks user if they prefer AMD or Nvidia via a dropdown menu
		 */
		String [] gpuChoice = {"AMD","Nvidia"};
		JFrame gpuFrame = new JFrame();
		/**
		 * Stores choice into specs[1]
		 */
		specs[1] = (String)JOptionPane.showInputDialog(gpuFrame, "Select a GPU", "Select GPU Brand", JOptionPane.QUESTION_MESSAGE, null, gpuChoice, gpuChoice[1]);
		/**
		 * Asks user if they prefer Western Digital or Seagate via a dropdown menu
		 */
		String [] driveChoice = {"Western Digital","Seagate"};
		JFrame driveFrame = new JFrame();
		/**
		 * Stores choice into specs[2]
		 */
		specs[2] = (String)JOptionPane.showInputDialog(cpuFrame, "Select a Hard Drive", "Select a Hard Drive Brand", JOptionPane.QUESTION_MESSAGE, null, driveChoice, driveChoice[1]);
		/**
		 * Asks user if they prefer Kingston or Corsair via a dropdown menu
		 */
		String [] memoryChoice = {"Kingston","Corsair"};
		JFrame memoryFrame = new JFrame();
		/**
		 * Stores choice into specs[3]
		 */
		specs[3] = (String)JOptionPane.showInputDialog(cpuFrame, "Select Memory", "Select Memory Brand", JOptionPane.QUESTION_MESSAGE, null, memoryChoice, memoryChoice[1]);
		return specs;
	}
	
}
