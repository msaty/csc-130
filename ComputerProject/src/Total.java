/*
 * Creates class Total for Computer to access and total the parts chosen
 */
public class Total {
	public double [] partsPrice = new double[4];
	public double partsTotal;
/**
 * Totals the cost of the computer components
 * @param components User's choices for computer parts
 * @return returns calculated total for the computer
 */
	public double calculateCost(String[] components) {
		if (components[0].equals("iCore 3 - $200")){
			partsPrice[0] = 200;
			components[0] = "iCore 3";
		}
		else if (components[0].equals("iCore 5 - $350")){
			partsPrice[0]= 350;
			components[0] = "iCore 5";
		}
		if (components[0].equals("A-Series - $150")){
			partsPrice[0] = 150;
			components[0] = "A-Series";
		}
		else if (components[0].equals("FX-Series - $300")){
			partsPrice[0]= 300;
			components[0] = "FX-Series";
		}
		if (components[1].equals("R-7 Series - $130")){
			partsPrice[1] = 130;
			components[1] = "R-7 Series";
		}
		else if (components[1].equals("R-9 Series - $300")){
			partsPrice[1]= 300;
			components[1] = "R-9 Series";
		}
		if (components[1].equals("760-Series - $350")){
			partsPrice[1] = 350;
			components[1] = "760-Series";
		}
		else if (components[1].equals("780-Series - $500")){
			partsPrice[1]= 500;
			components[1] = "780-Series";
		}
		if (components[2].equals("Green Series - $150")){
			partsPrice[2] = 150;
			components[2] = "Green Series";
		}
		else if (components[2].equals("Black Series - $250")){
			partsPrice[2]= 250;
			components[2] = "Black Series";
		}
		if (components[2].equals("HDD - $180")){
			partsPrice[2] = 180;
			components[2] = "HDD";
		}
		else if (components[2].equals("SSHD - $230")){
			partsPrice[2]= 230;
			components[2] = "SSHD";
		}
		if (components[3].equals("Hyper X 8GB - $90")){
			partsPrice[3] = 90;
			components[3] = "Hyper X 8GB";
		}
		else if (components[3].equals("Hyper X 16GB - $180")){
			partsPrice[3]= 180;
			components[3] = "Hyper X 16GB";
		}
		if (components[3].equals("XMS3 8GB - $130")){
			partsPrice[3] = 130;
			components[3] = "XMS3 8GB";
		}
		else if (components[3].equals("XMS3 16GB - $230")){
			partsPrice[3]= 500;
			components[3] = "XMS3 16GB";
		}
		/**
		 * calculates the total for all the parts selected and returns them
		 */
		partsTotal = partsPrice[0]+partsPrice[1]+partsPrice[2]+partsPrice[3];
		return partsTotal;
	}
	
}
