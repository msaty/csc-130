import javax.swing.JFrame;
import javax.swing.JOptionPane;
/**
 * creates public class Parts for class Computer to access and ask the user which parts they would like in their computer
 * @author Matt
 *
 */

public class Parts {
	public String [] parts = new String[8];
/**
 * 
 * @param specs uses array Specs to determine which computer components to show in the drop down menu
 * @return returns choices stored in array parts
 */
	
	public String[] pickParts(String [] specs) {
		/**
		 * If the user chooses Intel a dropdown with Intel processors is shown
		 */
		if (specs[0].equals("INTEL")){
			String [] intelCpuChoice = {"iCore 3 - $200", "iCore 5 - $350"};
			JFrame intelCpuFrame = new JFrame();
			/**
			 * Stores the user's choice in parts[0]
			 */
			parts[0] = (String)JOptionPane.showInputDialog(intelCpuFrame, "Select a CPU", "Select Intel CPU", JOptionPane.QUESTION_MESSAGE, null, intelCpuChoice, intelCpuChoice[1]);
		}
		/**
		 * If the user chooses AMD a dropdown with AMD processors is shown
		 */
		else if (specs[0].equals("AMD")){
			String [] amdCpuChoice = {"A-Series - $150", "FX-Series - $300"};
			JFrame amdCpuFrame = new JFrame();
			/**
			 * Stores user choice in parts[0]
			 */
			parts[0] = (String)JOptionPane.showInputDialog(amdCpuFrame, "Select a CPU", "Select AMD CPU", JOptionPane.QUESTION_MESSAGE, null, amdCpuChoice, amdCpuChoice[1]);
		}
		/**
		 * If the user chooses AMD a dropdown with AMD processors is shown
		 */
		if (specs[1].equals("AMD")){
			String [] amdGpuChoice = {"R-7 Series - $130", "R-9 Series - $300"};
			JFrame amdGpuFrame = new JFrame();
			/**
			 * Stores user choice in parts[1]
			 */
			parts[1] = (String)JOptionPane.showInputDialog(amdGpuFrame, "Select a GPU", "Select AMD GPU", JOptionPane.QUESTION_MESSAGE, null, amdGpuChoice, amdGpuChoice[1]);
			
		}
		/**
		 * If the user chooses Nvidia a dropdown with Nvidia processors is shown
		 */
		if (specs[1].equals("Nvidia")){
			String [] nvidiaGpuChoice = {"760-Series - $350", "780-Series - $500"};
			JFrame nvidiaGpuFrame = new JFrame();
			/**
			 * Stores user choice in parts[1]
			 */
			parts[1] = (String)JOptionPane.showInputDialog(nvidiaGpuFrame, "Select a GPU", "Select Nvidia GPU", JOptionPane.QUESTION_MESSAGE, null, nvidiaGpuChoice, nvidiaGpuChoice[1]);
			
		}
		/**
		 * If the user chooses Western Digital a dropdown with Western Digital harddrives is shown
		 */
		if (specs[2].equals("Western Digital")){
			String [] westDriveChoice = {"Green Series - $150", "Black Series - $250"};
			JFrame westDriveFrame = new JFrame();
			/**
			 * Stores user choice in parts[2]
			 */
			parts[2] = (String)JOptionPane.showInputDialog(westDriveFrame, "Select a Hard Drive", "Select Western Digital Drive", JOptionPane.QUESTION_MESSAGE, null, westDriveChoice, westDriveChoice[1]);
			
		}
		/**
		 * If the user chooses Seagate a dropdown with Seagate harddrives is shown
		 */
		if (specs[2].equals("Seagate")){
			String [] seaDriveChoice = {"HDD - $180", "SSHD - $230"};
			JFrame seaDriveFrame = new JFrame();
			/**
			 * Stores user choice in parts[2]
			 */
			parts[2] = (String)JOptionPane.showInputDialog(seaDriveFrame, "Select a Hard Drive", "Select Seagate Drive", JOptionPane.QUESTION_MESSAGE, null, seaDriveChoice, seaDriveChoice[1]);
			
		}
		/**
		 * If the user chooses Kingston a dropdown with Kingston memory is shown
		 */
		if (specs[3].equals("Kingston")){
			String [] kingMemoryChoice = {"Hyper X 8GB - $90", "Hyper X 16GB - $180"};
			JFrame kingMemoryFrame = new JFrame();
			/**
			 * Stores user choice in parts[3]
			 */
			parts[3] = (String)JOptionPane.showInputDialog(kingMemoryFrame, "Select Memory", "Select an amount of memory", JOptionPane.QUESTION_MESSAGE, null, kingMemoryChoice, kingMemoryChoice[1]);
			
		}
		/**
		 * If the user chooses Corsair a dropdown with Corsair memory is shown
		 */
		if (specs[3].equals("Corsair")){
			String [] coreMemoryChoice = {"XMS3 8GB - $130", "XMS3 16GB - $230"};
			JFrame coreMemoryFrame = new JFrame();
			/**
			 * Stores user choice in parts[3]
			 */
			parts[3] = (String)JOptionPane.showInputDialog(coreMemoryFrame, "Select a Hard Drive", "Select Seagate Drive", JOptionPane.QUESTION_MESSAGE, null, coreMemoryChoice, coreMemoryChoice[1]);
			
		}
		/**
		 * Returns part to main
		 */
		return parts;
	}
	
	
	
}
