import javax.swing.JFrame;
import javax.swing.JOptionPane;
/**
 * 
 * @author Matt
 *
 */
public class Computer {

	public static void main(String[] args) {
		/**
		 * Prompts the user to enter their information
		 */
		Register Resgister1 = new Register();
		String [] user = Resgister1.Registeruser();
		/**
		 * Logs in user to the system by prompting them for username and password
		 */
		Authentic Authentic1 = new Authentic();
		String password = Authentic.Authentication(user);
		/**
		 * continues on if password matches username
		 */
		if (password.equals(user[1])){
				/**
				 * Asks user to pick brand preferences for their components selection
				 */
				Preferences Preferences1 = new Preferences();
				String [] specs = Preferences1.pickSpecs();
				/**
				 * Asks user to pick their pc components based on their brand selections
				 */
				Parts Parts1 = new Parts();
				String [] components = Parts1.pickParts(specs); 
				/**
				 * Totals up the prices of the pc components
				 */
				Total Total1 = new Total();
				double cost = Total1.calculateCost(components);
				/**
				 * Prints out the user's shipping information, pc configuration, and total price
				 */
				Print Print1 = new Print();
				Print1.writeOutput(user,components, cost);

				System.exit(0);
		}
	}
		
	}
		
	

