import java.util.Scanner;
public class ComputerConfig {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		double compPrice = 499.00;
		double procPrice, ramPrice, hdPrice, audioPrice, videoPrice, speakerPrice;
		
		System.out.println("A system that would sufficiently serve a student's needs \n"
				+ "is a Dell Inspiron 3000 with an i3 Processor, Windows 8.1, 8GB of Memory\n"
				+ "and a 1TB Hard Drive. For a supremely awesome computer, fill in the prices\n"
				+ "for the prompted components to what it would cost you.");

		System.out.println("Type in the price for an i5 processor, "
				+ " then press the ENTER key.");
		procPrice=input.nextDouble();
		System.out.println("Type in the price for 16GB of RAM, "
				+ " then press the ENTER key.");
		ramPrice=input.nextDouble();
		System.out.println("Type in the price for a 2TB hard drive, "
				+ " then press the ENTER key.");
		hdPrice=input.nextDouble();
		System.out.println("Type in the price for a sound blaster card, "
				+ " then press the ENTER key.");
		audioPrice=input.nextDouble();
		System.out.println("Type in the price for an nvidia GeForce 780, "
				+ " then press the ENTER key.");
		videoPrice=input.nextDouble();
		System.out.println("Type in the price for logitech Z63 speakers, "
				+ " then press the ENTER key.");
		speakerPrice=input.nextDouble();
		
		compPrice = compPrice + procPrice + ramPrice + hdPrice + audioPrice + videoPrice + speakerPrice;
				
		System.out.println("Your new beast computer will cost: "+compPrice);
	}

}
