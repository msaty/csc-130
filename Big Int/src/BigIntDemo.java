import java.util.Scanner;

public class BigIntDemo

{

    public static void main(String[] args)

    {

      //BigInt b1 = new BigInt("1000000");

      //BigInt b1 = new BigInt("+0");

      //BigInt b1 = new BigInt("-0");

      //BigInt b1 = new BigInt("5stt7");// error

      //BigInt b1 = new BigInt("+");// error

      //BigInt b1 = new BigInt("-");// error

      BigInt b1 = new BigInt("000500");// acceptable == 500

     //BigInt b1 = new BigInt("   000500");// error

           // BigInt b1 = new BigInt("44444444445555555555666666666677777777770000000000");

        System.out.println("b1 is " + b1);

    }

}