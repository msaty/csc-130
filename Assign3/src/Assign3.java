/* Psuedocode
Start
Attempt = 0
User = msaty
correctPassword = 1234
correctAccount = admin
username = "";
String password = "";
String account = "";

Dowhile validuser /= msaty, attempt > 3
Prompt user for username
Attempt = Attempt +1
IF username = msaty
Dowhile password =/=1234,  count > 3
			Prompt user for password
			Attempt = Attempt +1
			IF password  = 1234
				Dowhile account type =/= admin
					Prompt user to select an account type
					IF account type = admin
						Display welcome message
					Break
		IF Count = 3
			Display Admin Message 
			Break
*/

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Assign3 {

public static void main(String[] args) {
		// TODO Auto-generated method stub
		//initialize values
		int attempt = 0;
		String correctUser = "msaty";
		String correctPassword = "1234";
		String correctAccount = "admin";
		String username = "";
		String password = "";
		String account = "";
		//Ask for username up to three times, at three display admin message
		while (attempt < 3 && !username.equalsIgnoreCase(correctUser)) {
			username = JOptionPane.showInputDialog(
					"Enter your username, then press the enter key:");
			attempt++;
			//if correct username is entered, program will as for password
			if (username.equalsIgnoreCase(correctUser))
				//Ask for password up to three times (includes attempts at password), at three display admin message
				while  (attempt < 3 && !password.equalsIgnoreCase(correctPassword)) {	
				password = JOptionPane.showInputDialog(
							"Enter your password, then press the ENTER key:");
				attempt++;
				//if correct password is enter, program will ask for account type and keep asking until correct one is selected
					if(password.equals(correctPassword)){
						JOptionPane.showMessageDialog(null, "Welcome msaty!");
					 while (!account.equalsIgnoreCase(correctAccount)){
						//pop up menu variables declared
						 String [] options = {"admin","staff","student"};
						JFrame frame = new JFrame("Input Dalog Example 3");
						account = (String)JOptionPane.showInputDialog(frame, "Select your Account Type","Select Account", JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
					//if correct account is selected, a welcome message is displayed and program ends
						if(account.equalsIgnoreCase(correctAccount))
							{
							JOptionPane.showMessageDialog(null, "Welcome admin!");
							break;
							}
					 }
					}
						
		}
		if (attempt == 3)
			JOptionPane.showMessageDialog(null, "Incorrect credentials, contact admin.");
	}

}
}

